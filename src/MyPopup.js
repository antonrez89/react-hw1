import React from 'react';
import './MyPopup.css';

class MyPopup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			post: null,
			error: null,
			isLoaded: false
		};
	}

	componentDidMount() {
		fetch("https://jsonplaceholder.typicode.com/posts")
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLoaded: true,
						post: result[Math.floor(Math.random() * (result.length + 1))]
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						post: null,
						error
					});
				}
			)
	}
	render() {
		const content = this.state.isLoaded ?
			<div><h3>{this.state.post.title}</h3><div>{this.state.post.body}</div></div>
			: 'Content is loading...';
		return (
			<div
				className="bPopup" >
				<div className="bPopup__closer" onClick={this.props.onClose}>X</div>
				Popup:
				<div className="bPopup__body">{content}</div>
			</div>
		);
	}
}

export default MyPopup;