import React from 'react';

class MyButton extends React.Component {
	render() {
		return (
			<button
				type="text"
				onClick={this.props.onClick}
			>
				Button: {this.props.text}
			</button>
		);
	}
}

export default MyButton;