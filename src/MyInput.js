import React from 'react';

class MyInput extends React.Component {
	render() {
		return (
			<div>
				<input
					value={this.props.value}
					onChange={(event) => this.props.onChange(event.target.value)} type="text"/>
			</div>
		);
	}
}

export default MyInput;