import React, {Component} from 'react';
import './App.css';
import MyInput from './MyInput';
import MyButton from "./MyButton";
import MyPopup from "./MyPopup";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: '',
			popupOpened: false
		};
	}

	onInputChange(newValue) {
		this.setState({
			text: newValue,
			popupOpened: this.state.popupOpened
		});
	}

	handleClick() {
		this.setState({
			text: this.state.text,
			popupOpened: !this.state.popupOpened
		});
	}

	closePopup() {
		this.setState({
			text: this.state.text,
			popupOpened: false
		});
	}

	render() {
		const popup = this.state.popupOpened? <MyPopup onClose={() => this.closePopup()}/> : '';
		return (
			<div className="App">
				<MyInput onChange={(value) => this.onInputChange(value)}/>
				<MyButton text={this.state.text} onClick={() => this.handleClick()}/>
				{popup}
			</div>
		);
	}
}

export default App;
